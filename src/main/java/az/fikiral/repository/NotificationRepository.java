package az.fikiral.repository;

import az.fikiral.model.entity.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {
    List<Notification> findAllByPostOwnerId(Long userId);
    List<Notification> findAllByPostId(Long postId);

}
