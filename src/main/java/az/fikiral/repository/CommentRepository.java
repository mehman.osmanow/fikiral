package az.fikiral.repository;

import az.fikiral.model.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query(value = "select count(*) from comments c where post_id=?1",nativeQuery = true)
    int getCommentCountOfPost(Long postId);

    int countAllByPostId(Long postId);


    @Query(value = "select * from comments c where post_id=?1",nativeQuery = true)
    List<Comment> getComment(Long post_id);
}
