package az.fikiral.repository;

import az.fikiral.model.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
     List<Post> getPostsByCategory_Id(Long id);
     List<Post> findAllByAccepted(boolean accepted);
     List<Post> getPostByContentContainingIgnoreCase(String content);
     @Query(value =
             """
             SELECT p.id, p.content, p.published_at, p.category_id, p.user_id, p.accepted, COUNT(l.id) AS like_count 
             FROM posts p 
             LEFT JOIN likes l ON p.id = l.post_id
             WHERE accepted = true
             GROUP BY p.id, p.content, p.published_at, p.category_id, p.user_id
             ORDER BY like_count DESC limit ?1""", nativeQuery = true)
     List<Post> getPopularPosts(int limit);


     @Query(value = "select post_id from saved_posts where user_id=?1", nativeQuery = true)
     List<Long> getAllSavedPostIDs(Long userID);

}