
package az.fikiral.repository;

import az.fikiral.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    boolean existsByName(String name);

    Optional<Category> getCategoryBySlug(String slug);

    Optional<Category> getCategoryByName(String name);

    @Query(value = "SELECT * FROM categories c WHERE id IN (SELECT category_id FROM interested_category WHERE user_id =?1)"
            , nativeQuery = true)
    List<Category> getSavedCategoriesByUserId(Long userId);

    @Query(value = "select category_id from interested_category where user_id=?1", nativeQuery = true)
    List<Long> getInterestedCategoriesId(Long userId);

}