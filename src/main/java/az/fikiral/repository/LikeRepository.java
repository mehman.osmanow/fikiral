package az.fikiral.repository;

import az.fikiral.model.entity.Comment;
import az.fikiral.model.entity.Like;
import az.fikiral.model.entity.Post;
import az.fikiral.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Mehman Osmanov 24.02.24
 * @project fikiral
 */
@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {
    @Query(value = "select post_id from likes l where user_id=?1 and is_liked=?2", nativeQuery = true)
    List<Long> getAllLikedOrDislikedPostsIDs(Long userID, boolean liked);
    int countAllByPostIdAndLiked(Long postId, boolean liked);
    int countAllByCommentIdAndLikedIsTrue(Long commentId);
    Optional<Like> findByUserAndPost(User user, Post post);
    Optional<Like> findByUserAndComment(User user, Comment comment);
}