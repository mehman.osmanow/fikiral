package az.fikiral.repository;

import az.fikiral.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserName(String userName);

    Optional<User> findByGmail(String mail);

    //    @Query(nativeQuery = true, value = "select * from users where id=?1 and is_active=true")
    Optional<User> findByIdAndActivatedIsTrue(Long id);

    List<User> findAllByActivatedIsTrue();
    List<User> findAllByDeletedFalse();

    boolean existsByGmail(String gmail);

    Optional<User> findByGmailAndActivatedIsTrue(String gmail);

    boolean existsByUserName(String userName);
}
