package az.fikiral.constant;

/**
 * @author Mehman Osmanov 03.04.24
 * @project fikiral
 */
public class Constants {
    public static final String BUCKET_NAME = "fikiral-bkt";
    public static final String REGION = "eu-central-1";
    public static final String OBJECT_KEY = "images/";
}
