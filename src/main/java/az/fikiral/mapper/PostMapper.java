package az.fikiral.mapper;

import az.fikiral.model.dto.request.PostRequest;
import az.fikiral.model.dto.response.PostResponse;
import az.fikiral.model.entity.Post;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PostMapper {
    @Mapping(target = "user.id", source = "userId")
    Post requestToEntity(PostRequest postRequest);
//    @Mapping(target = "userResponse", source = "user" )
    PostResponse entityToResponse(Post post, int likeCount, int commentCount);
}
