
package az.fikiral.mapper;

import az.fikiral.model.dto.request.CategoryRequest;
import az.fikiral.model.dto.response.CategoryResponse;
import az.fikiral.model.entity.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoryMapper {
    Category requestToEntity(CategoryRequest request);
    @Mapping(target = "postCount", expression = "java(category.getPosts().size())")
    CategoryResponse entityToResponse(Category category);
}
