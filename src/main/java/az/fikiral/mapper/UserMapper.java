package az.fikiral.mapper;

import az.fikiral.model.dto.request.UserRequest;
import az.fikiral.model.dto.response.UserFullResponse;
import az.fikiral.model.entity.User;
import az.fikiral.model.dto.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    User requestToEntity(UserRequest userRequest);

    @Mapping(target = "postCount", expression = "java(user.getCreatedPosts().size())")
    UserResponse entityToResponse(User user);
    UserFullResponse entityToLoginResponse(User user);
}
