package az.fikiral.mapper;

import az.fikiral.model.dto.request.CommentRequest;
import az.fikiral.model.dto.response.CommentResponse;
import az.fikiral.model.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CommentMapper {
    Comment requestToEntity(CommentRequest commentRequest);
    CommentResponse entityToResponse(Comment comment, int likeCount);


}
