package az.fikiral.mapper;

import az.fikiral.model.dto.request.NotificationRequest;
import az.fikiral.model.dto.response.NotificationResponse;
import az.fikiral.model.entity.Notification;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Mapper(componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NotificationMapper {
    Notification requestToEntity(NotificationRequest request);
    @Mapping(target = "id", source = "notifyId")
    NotificationResponse entityToResponse(Notification notification);

}
