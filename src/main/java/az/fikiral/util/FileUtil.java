package az.fikiral.util;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetUrlRequest;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.io.InputStream;

@Component
@Slf4j
@RequiredArgsConstructor
public class FileUtil {

    private final S3Client s3Client;

    @SneakyThrows
    public String uploadImage(String bucketName, String objectKey, MultipartFile multipartFile) {
        if (multipartFile.getSize() > 4_000_000) {
            throw new MaxUploadSizeExceededException(multipartFile.getSize());
        }
        InputStream inputStream = multipartFile.getInputStream();
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(objectKey)
                .build();
        s3Client.putObject(request, RequestBody.fromInputStream(inputStream, multipartFile.getSize()));
        GetUrlRequest urlRequest = GetUrlRequest.builder().bucket(bucketName ).key(objectKey).build();
        return s3Client.utilities().getUrl(urlRequest).toExternalForm();
    }

}