package az.fikiral.config;

import az.fikiral.security.JwtAuthenticationFilter;
import az.fikiral.security.PasswordEncoder;
import az.fikiral.service.impl.UserDetailServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@RequiredArgsConstructor
//@EnableGlobalMethodSecurity(
//        jsr250Enabled = true,
//        securedEnabled = true,
//        prePostEnabled = true
//)
public class SecurityConfig {

   private final JwtAuthenticationFilter jwtAuthenticationFilter;
   private final UserDetailServiceImpl userDetailsService;
   private final PasswordEncoder passwordEncoder;

   private static final String[] AUTH_WHITELIST = {
           "/api/v1/auth/**",
           "/v3/api-docs/**",
           "/v3/api-docs.yaml",
           "/swagger-ui/**",
           "/swagger-ui.html",
           "/swagger-resources/**",
           "/webjars/**",
           "/v3/**",
           "/swagger/**"
   };

   @Bean(BeanIds.AUTHENTICATION_MANAGER)
   public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
      return authenticationConfiguration.getAuthenticationManager();
   }

   @Bean
   public AuthenticationProvider authenticationProvider() {
      DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
      authProvider.setUserDetailsService(userDetailsService);
      authProvider.setPasswordEncoder(passwordEncoder.getPasswordEncoder());
      return authProvider;
   }

   @Bean
   public CorsConfigurationSource corsConfigurationSource() {
      CorsConfiguration configuration = new CorsConfiguration();
      configuration.addAllowedOrigin("*"); // Allow all origins
      configuration.addAllowedMethod("*"); // Allow all HTTP methods
      configuration.addAllowedHeader("*"); // Allow all headers

      UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
      source.registerCorsConfiguration("/**", configuration);

      return source;
   }

   @Bean
   public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

      http
              .csrf(AbstractHttpConfigurer::disable)
              .cors(httpSecurityCorsConfigurer -> httpSecurityCorsConfigurer.configurationSource(corsConfigurationSource()))
//              .cors(Customizer.withDefaults())
              .sessionManagement(httpSecuritySessionManagementConfigurer ->
                      httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
              .authorizeHttpRequests(auth ->
                      auth.requestMatchers("/v1/auth/**","/v1/user/**","/v1/think/**","/v1/verify/**",
                              "/v1/category/**","/v1/comment/**","/v1/like/**", "/v1/notification/**").permitAll())
              .authorizeHttpRequests(auth ->
                      auth.requestMatchers(AUTH_WHITELIST).permitAll())
              .authorizeHttpRequests(auth ->
                      auth.anyRequest().authenticated())
              .authenticationProvider(authenticationProvider())
              .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

      return http.build();
   }
}