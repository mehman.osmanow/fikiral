package az.fikiral.controller;

import az.fikiral.model.dto.request.CommentRequest;
import az.fikiral.model.dto.response.CommentResponse;
import az.fikiral.repository.CommentRepository;
import az.fikiral.service.CommentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mehman Osmanov 15.02.24
 * @author Isgandar Mammadov 22.02.24
 * @project fikiral
 */
@RestController
@RequestMapping("/v1/comment")
@RequiredArgsConstructor
public class CommentController {
    //todo implement methods
    private final CommentRepository commentRepository;
    private final CommentService commentService;


    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created"),
            @ApiResponse(code = 404, message = "Not found"),
    })
    @ApiOperation(value = "Create new comment", notes = "Comment must be unique")
    @PostMapping("/")
    public ResponseEntity<String> createComment(@ApiParam(name = "request", value = "Category request object", required = true)
                                                @Valid @RequestBody CommentRequest request) {
        commentService.createComment(request);
        return ResponseEntity.status(HttpStatus.CREATED).body("Succesfully created");
    }

    @ApiOperation(value = "Get all comments", notes = "Get all comments")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All comments returned"),
            @ApiResponse(code = 404, message = "Comment not found")
    })
    @GetMapping("/{id}")
    public List<CommentResponse> getAllCommentsOfPost(@ApiParam(name = "request", value = "Comment request object", required = true)
                                                      @PathVariable Long id) {
        return commentService.getAllComments(id);
    }

    public CommentResponse updateComment(Long userId, Long commentId) {

        commentRepository.findById(commentId);
        return null;
    }

    @ApiOperation(value = "Delete comment by id", notes = "Delete comment by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Comment deleted successfully"),
            @ApiResponse(code = 404, message = "Comment id not found"),
            @ApiResponse(code = 409, message = "Could not execute statement")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable Long id) {
        commentService.deleteComment(id);
        return ResponseEntity.ok("Comment deleted");
    }

}