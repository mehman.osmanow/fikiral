package az.fikiral.controller;

import az.fikiral.model.dto.request.NotificationRequest;
import az.fikiral.model.dto.response.NotificationResponse;
import az.fikiral.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@RestController
@RequestMapping("/v1/notification")
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;

    @PostMapping("/")
    public void createNotification(@RequestBody NotificationRequest notificationRequest){
        notificationService.createNotification(notificationRequest);
    }

    @GetMapping("/{userId}")
    public List<NotificationResponse> getNotifications(@PathVariable Long userId){
        return notificationService.getAllNotification(userId);
    }

    @DeleteMapping("/{id}")
    public void deleteNotification(@PathVariable Long id) {
        notificationService.deleteNotification(id);
    }

}
