package az.fikiral.controller;

import az.fikiral.model.dto.request.LikeCommentRequest;
import az.fikiral.model.dto.request.LikePostRequest;
import az.fikiral.model.dto.response.ErrorResponse;
import az.fikiral.service.LikeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mehman Osmanov 24.02.24
 * @project fikiral
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/like")
@Tag(name = "Like", description = "Like Apis")
public class LikeController {

    private final LikeService likeService;


//    @Operation(summary = "Get likes count of the post", description = "Like post")
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = Integer.class))}),
//            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
//    @GetMapping("/{postId}")
//    public Integer getLikesCount(@PathVariable Long postId){
//        return likeService.getLikeCountByPostId(postId);
//    }

    @Operation(summary = "Like post", description = "Like post")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/post/")
    public void likePost(@RequestBody LikePostRequest request){
        likeService.likePost(request);
    }

    @Operation(summary = "Like comment", description = "Like comment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/comment/")
    public void likeComment(@RequestBody LikeCommentRequest request){
        likeService.likeComment(request);
    }
}
