package az.fikiral.controller;

import az.fikiral.service.VerifyAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@RestController
@RequestMapping("/v1/verify")
@RequiredArgsConstructor
public class VerifyController {

    private final VerifyAccountService verifyService;

    @GetMapping("/{token}")
    public String verify(@PathVariable String token){
        return verifyService.verify(token);
    }

    @PutMapping("/activated")
    public boolean isActivated(@RequestParam String gmail){
        return verifyService.isActivated(gmail);
    }

}
