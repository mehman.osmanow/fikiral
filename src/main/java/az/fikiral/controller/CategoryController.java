
package az.fikiral.controller;

import az.fikiral.model.dto.request.CategoryRequest;
import az.fikiral.model.dto.response.CategoryResponse;
import az.fikiral.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

@RestController
@Validated
@RequestMapping("/v1/category")
@RequiredArgsConstructor
@Tag(name = "Category", description = "Category Apis")
public class CategoryController {
    private final CategoryService service;

    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successfully created"),
            @ApiResponse(responseCode = "404", description = "Not found"),
    })
    @Operation(summary = "Create new category", description = "Category name must be unique")
    @PostMapping("/")
    public ResponseEntity<String> addCategory(@Parameter(name = "request", description = "Category request object", required = true)
                                              @Valid @RequestBody CategoryRequest request) {
        service.createCategory(request);
        return ResponseEntity.status(HttpStatus.CREATED).body("Successfully created");
    }

    @Operation(summary = "Update category by id", description = "Update category")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated"),
            @ApiResponse(responseCode = "404", description = "Category id not found")
    })
    @PutMapping("/{id}")
    public void updateCategory(@Valid @PathVariable Long id, @RequestBody CategoryRequest request) {
        service.updateCategory(id, request);
    }

    @Operation(summary = "Get all categories", description = "Get all categories")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All category returned"),
            @ApiResponse(responseCode = "404", description = "Category not found")
    })
    @GetMapping("/")
    public ResponseEntity<List<CategoryResponse>> getAllCategory() {
        return ResponseEntity.ok(service.getAllCategory());
    }

    @Operation(summary = "Get category by id", description = "Get category by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Category returned by id"),
            @ApiResponse(responseCode = "404", description = "Category id not found"),
    })
    @GetMapping("/{id}")
    public ResponseEntity<CategoryResponse> getCategoryById(@PathVariable Long id) {
        return ResponseEntity.ok(service.getCategoryById(id));
    }

    //todo changed getInterestedCategory method place
    @Operation(summary = "Get interested category of user", description = "Get interested category of user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema())})})
    @GetMapping("/user/{userId}")
    public List<CategoryResponse> getInterestedCategory(@PathVariable Long userId) {
        return service.getSavedCategoryByUserId(userId);
    }

    @Operation(summary = "Delete category by id", description = "Delete category by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Category deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Category id not found"),
            @ApiResponse(responseCode = "409", description = "Could not execute statement")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCategory(@PathVariable Long id) {
        service.deleteCategoryById(id);
        return ResponseEntity.ok("Category with ID " + id + " has been successfully deleted.");
    }
}
