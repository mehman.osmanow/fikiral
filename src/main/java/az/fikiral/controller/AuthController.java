
package az.fikiral.controller;

import az.fikiral.model.dto.request.ChangePasswordRequest;
import az.fikiral.model.dto.request.LoginRequest;
import az.fikiral.model.dto.request.UserRequest;
import az.fikiral.model.dto.response.ErrorResponse;
import az.fikiral.model.dto.response.AuthResponse;
import az.fikiral.model.dto.response.TokenResponse;
import az.fikiral.model.dto.response.UserResponse;
import az.fikiral.service.AuthService;
import az.fikiral.service.VerifyAccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@RequestMapping("/v1/auth")
@RestController
@RequiredArgsConstructor
@Tag(name = "Authentication", description = "Authentication Apis")
public class AuthController {

    private final AuthService authService;
    private final VerifyAccountService verifyService;


    @Operation(summary = "Login user with LoginRequest", description = "Login user with LoginRequest")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = AuthResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@Valid @RequestBody LoginRequest request) {
        return ResponseEntity.accepted().body(authService.login(request));
    }

    @Operation(summary = "Refresh bearer token", description = "Refresh bearer token")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = TokenResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/refresh")
    public ResponseEntity<TokenResponse> refreshToken(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(authService.refreshJWT(userDetails));
    }

    @Operation(summary = "Adding user with UserRequest", description = "Adding user for UserRequest")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = UserResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/registration")
    public void registration(@Valid @RequestBody UserRequest request) throws IOException {
        authService.registration(request);
    }

    @SneakyThrows
    @PutMapping("/forgot-password")
    public ResponseEntity<String> forgotPassword(@RequestParam String email){
        return ResponseEntity.ok(authService.forgotPassword(email));
    }



    @PutMapping("/change-password")
    public void changePassword(@RequestBody @Valid ChangePasswordRequest request){
        authService.changePassword(request);
    }


}
