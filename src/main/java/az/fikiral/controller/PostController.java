package az.fikiral.controller;

import az.fikiral.model.dto.request.PostRequest;
import az.fikiral.model.dto.response.ErrorResponse;
import az.fikiral.model.dto.response.PostResponse;
import az.fikiral.repository.PostRepository;
import az.fikiral.service.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/think")
@Tag(name = "Post", description = "Post Apis")
public class PostController {

    private final PostService postService;
    private final PostRepository postRepository;

    @Operation(summary = "Adding post", description = "Adding post for fikiral user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/")
    public void createPost(@Valid @RequestBody PostRequest postRequest) {
        postService.createPost(postRequest);
    }


    @Operation(summary = "Save post", description = "Save post")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PutMapping("/save/")
    public List<Long> savePost(@RequestParam Long userId, @RequestParam Long postId) {
        return postService.savePost(userId, postId);
    }

    @Operation(summary = "Retrieve all accepted posts", description = "Retrieve all posts. The response is the list of posts")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/accepted/")
    public List<PostResponse> getAllAcceptedPosts() {
        return postService.getAllAcceptedPosts();
    }


    @Operation(summary = "Retrieve all in-accepted posts", description = "Retrieve all posts. The response is the list of posts")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/in-accepted/")
    public List<PostResponse> getAllInAcceptedPosts() {
        return postService.getAllInAcceptedPosts();
    }


    @Operation(summary = "Retrieve all posts by category group that provided category ID.", description = "Retrieve all posts by category group that provided category ID.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/categories/")
    public List<List<PostResponse>> getPostsByCategoryIds(@RequestParam List<Long> categoryIds) {
        return postService.getPostsByCategoryIds(categoryIds);
    }

    @Operation(summary = "Retrieve a post by category",
            description = "Get a posts object by specifying its category. The response is the list of the post object")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    //todo change input parameter
    @GetMapping("/category/")
    public List<PostResponse> getPostByCategory(@RequestParam String slug) {
        return postService.getPostsByCategorySlug(slug);
    }

    @Operation(summary = "Retrieve most liked posts", description = "Retrieve most liked posts")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/popular")
    public List<PostResponse> getPopularPosts(@RequestParam int limit) {
        return postService.getPopularPosts(limit);
    }

    @Operation(summary = "Retrieve a post by content",
            description = "Get a posts object by specifying its content. The response is the list of the post object")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = PostResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/content/")
    public List<PostResponse> findPostByContent(@RequestParam String content) {
        return postService.findPostsByContent(content);
    }

    @Operation(summary = "Update a post by id", description = "Get a posts object by specifying its id.")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/")
    public void updatePost(@RequestParam String content, @RequestParam Long id) {
        postService.updatePost(content, id);
    }

    @Operation(summary = "Accept a post by id", description = "Accept a posts object by specifying its id.")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/accept/{id}")
    public void acceptPost(@PathVariable Long id) {
        postService.acceptPost(id);
    }

    @Operation(summary = "Delete a post by id", description = "Delete a posts object by specifying its id.")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable Long id) {
        postService.deletePost(id);
    }

}
