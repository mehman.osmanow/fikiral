package az.fikiral.controller;

import az.fikiral.model.dto.request.UserUpdateRequest;
import az.fikiral.model.dto.response.ErrorResponse;
import az.fikiral.model.dto.response.UserFullResponse;
import az.fikiral.model.dto.response.UserResponse;
import az.fikiral.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Slf4j
@RestController
@RequestMapping("/v1/user")
@RequiredArgsConstructor
@Tag(name = "User", description = "User Apis")
public class UserController {

    private final UserService userService;

    @Operation(
            summary = "Adding user profile image providing image file and user id",
            description = "Adding user profile image providing image file and user id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "400", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")})})
    @PostMapping("/image")
    public ResponseEntity<String> addProfileImage(@RequestBody MultipartFile image, Long id) throws IOException {
        userService.addImage(image, id);
        log.info("Successfully added image");
        return new ResponseEntity<>("Successfully added image", HttpStatus.CREATED);

    }

    @Operation(
            summary = "Retrieve a User by Id",
            description = "Get a User object by specifying its id. The response is User object with id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = UserResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/{id}")
    public ResponseEntity<UserFullResponse> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @Operation(summary = "Retrieve all Users", description = "Get  all Users object ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/")
    public ResponseEntity<List<UserResponse>> getAllUser() {
        return ResponseEntity.ok(userService.getAllUser());
    }

    @Operation(summary = "Retrieve all Users", description = "Get  all Users object ")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = List.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/all/")
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @Operation(summary = "Check the user's gmail.", description = "Check the user's gmail to see if it exists.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = Boolean.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/check/mail/")
    public ResponseEntity<Boolean> checkUserGmail(@RequestParam String gmail) {
        return ResponseEntity.ok(userService.checkUserGmail(gmail));
    }

    @Operation(summary = "Check the username.", description = "Check the username to see if it exists.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = Boolean.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/check/username/")
    public ResponseEntity<Boolean> checkUserName(@RequestParam String userName) {
        return ResponseEntity.ok(userService.checkUserName(userName));
    }

    @Operation(summary = "Update a User by Id",
            description = "Update a User object by specifying its id. The response is User object with updated version")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/{id}")
    public void updateUser(@RequestBody UserUpdateRequest userRequest, @PathVariable Long id) {
        userService.updateUser(userRequest, id);
    }

    @Operation(summary = "Delete a User by Id", description = "Delete a User object by specifying its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/{id}")
    public void deleteAccount(@PathVariable Long id) {
        userService.deleteUserById(id);
    }

    @Operation(summary = "Delete a User by Id", description = "Delete a User object by specifying its id")
    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema(implementation = ErrorResponse.class), mediaType = "app/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PutMapping("/restore/{id}")
    public void blockAccount(@PathVariable Long id) {
        userService.blockedUnlocked(id);
    }
}