package az.fikiral.exception;

import az.fikiral.model.dto.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.awt.image.ImagingOpException;
import java.time.LocalDateTime;
import java.util.InputMismatchException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(value = {
            AccessForbidden.class,
            AlreadyExistsException.class,
            InappropriateTransfer.class,
            InsufficientBalanceException.class,
            InputMismatchException.class,
            ImagingOpException.class,
            MethodArgumentNotValidException.class,
            NotActive.class,
            NotAnyAccountAddedYet.class,
            NotFoundException.class,
            WrongPasswordException.class
    })
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(getHttpStatus(ex).value());
        errorResponse.setErrorMessage(ex.getMessage());
        errorResponse.setDate(LocalDateTime.now());
        errorResponse.setStatus(getHttpStatus(ex));
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }

    /*
    * AccessForbidden.class,
                AlreadyExistsException.class,
                InappropriateTransfer.class,
                InsufficientBalanceException.class,
                NotActive.class,
                NotAnyAccountAddedYet.class,
                NotFoundException.class,
                ImagingOpException.class,
    * */
    private HttpStatus getHttpStatus(Exception ex) {
        if (ex instanceof AccessForbidden || (ex instanceof WrongPasswordException)) {
            return HttpStatus.FORBIDDEN;
        } else if ((ex instanceof AlreadyExistsException) || (ex instanceof InappropriateTransfer) || (ex instanceof NotActive)) {
            return HttpStatus.CONFLICT;
        } else if (ex instanceof InsufficientBalanceException) {
            return HttpStatus.PAYMENT_REQUIRED;
        } else if ((ex instanceof NotAnyAccountAddedYet) || (ex instanceof NotFoundException)) {
            return HttpStatus.NOT_FOUND;
        } else if ((ex instanceof MethodArgumentNotValidException) || (ex instanceof InputMismatchException)) {
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}