package az.fikiral.exception;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InsufficientBalanceException extends RuntimeException {
    final HttpStatus status = HttpStatus.PAYMENT_REQUIRED;
    final String message;
}
