package az.fikiral.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class NotActive extends RuntimeException{
    final HttpStatus status = HttpStatus.CONFLICT;
    final String message;
}
