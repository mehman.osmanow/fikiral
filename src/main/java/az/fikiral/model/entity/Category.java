
package az.fikiral.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "categories")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   Long id;

   @Column(name = "name", unique = true, nullable = false)
   String name;
   @Column(name = "slug", unique = true, nullable = false)
   String slug;

   @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
   List<Post> posts;

//   @ManyToMany(mappedBy = "interestedCategory")
//   @JsonBackReference
//   List<User> users;

}

