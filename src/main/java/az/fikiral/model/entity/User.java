package az.fikiral.model.entity;

import az.fikiral.model.enums.RoleType;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.aspectj.weaver.ast.Not;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Mehman Osmanov 05.02.24
 * @project fikiral
 */

@Setter
@Getter
@Entity(name = "users")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(unique = true, nullable = false)
    String userName;
    @Column(unique = true, nullable = false)
    String gmail;
    @Column(nullable = false)
    String password;
    String image;
    boolean blocked;
    boolean activated;
    boolean deleted;
    @CreationTimestamp
    LocalDateTime registeredAt;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role_type", nullable = false)
    RoleType roleType;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Post> createdPosts = new ArrayList<>();

//    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
//    private List<Notification> notifications = new ArrayList<>();

    @ManyToMany
//    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinTable(
            name = "saved_posts",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id"))
    private List<Post> savedPosts;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    List<Comment> comments;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    List<Like> likes;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "interested_category",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    List<Category> interestedCategory;


    public List<Category> getInterestedCategory() {
        if (interestedCategory == null) {
            interestedCategory = new ArrayList<>();
        }
        return interestedCategory;
    }
}
