package az.fikiral.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mehman Osmanov 05.02.24
 * @project fikiral
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity(name = "posts")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(nullable = false)
    String content;
    boolean accepted;

    @CreationTimestamp
    LocalDateTime publishedAt;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;

    @ManyToMany(mappedBy = "savedPosts")
    @JsonBackReference
    List<User> users;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    Category category;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "post")
    List<Comment> comments;

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "post")
    List<Like> likes;

    @PreRemove
    private void removeFromUsers() {
        if (users != null) {
            for (User user : users) {
                user.getSavedPosts().remove(this);
            }
        }
    }
}