package az.fikiral.model.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 13.02.24
 * @project fikiral
 */
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "likes")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "is_liked")
    boolean liked;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;

    @ManyToOne
    @JoinColumn(name = "post_id", referencedColumnName = "id")
    Post post;

    @ManyToOne
    @JoinColumn(name = "comment_id", referencedColumnName = "id")
    Comment comment;

    public Like(User user, Post post, boolean liked) {
        this.liked = liked;
        this.user = user;
        this.post = post;
    }
    public Like(User user, Comment comment, boolean liked) {
        this.liked = liked;
        this.user = user;
        this.comment = comment;
    }
}
