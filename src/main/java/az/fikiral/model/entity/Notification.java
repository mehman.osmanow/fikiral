package az.fikiral.model.entity;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Entity(name = "notifications")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long notifyId;

    Long postId;
    Long postOwnerId;
    Long actionOwnerId;
    @Column
    String action;

//    @ManyToOne
//    @JoinColumn(name = "action_owner_id", referencedColumnName = "id")
//    User user;

}
