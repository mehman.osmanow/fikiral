package az.fikiral.model.enums;
public enum TokenType {
    ACCESS_TOKEN,
    REFRESH_TOKEN
}