/*
 *Created by Jaweed.Hajiyev
 *Date:19.08.23
 *TIME:20:21
 *Project name:LMS
 */

package az.fikiral.model.enums;

public enum RoleType {
    ADMIN,
    USER,
    GUEST
}
