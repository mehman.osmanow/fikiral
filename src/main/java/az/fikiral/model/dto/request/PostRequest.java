package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;

/**
 * @author Mehman Osmanov 05.02.24
 * @project fikiral
 */
@Data
public class PostRequest {
    @NotBlank
    @Schema(name = "content", example = "Some post content", requiredMode = Schema.RequiredMode.REQUIRED)
    String content;
    @Positive
    @NotNull
    @Schema(name = "categoryId", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    long categoryId;
    @Positive
    @NotNull
    @Schema(name = "userId", example = "1", requiredMode = Schema.RequiredMode.REQUIRED)
    long userId;

}