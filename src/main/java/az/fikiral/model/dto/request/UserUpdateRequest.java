package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author Mehman Osmanov 16.03.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserUpdateRequest {
    @Schema(name = "userName", example = "john07")
    String userName;

    @Email(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$", message = "Invalid email address")
    @Schema(name = "gmail", example = "example@gmail.com")
    String gmail;

    @Schema(name = "categories", example = "[1, 3, 6]")
    List<Long> categories;
}
