package az.fikiral.model.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NotificationRequest {
    Long postId;
    Long postOwnerId;
    Long actionOwnerId;
    String action;
}
