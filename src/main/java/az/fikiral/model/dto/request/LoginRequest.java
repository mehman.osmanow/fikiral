package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Nullable;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginRequest {
    @Schema(name = "gmail", example = "example@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    String gmail;
    @Schema(name = "password", example = "Example123", requiredMode = Schema.RequiredMode.REQUIRED)
    String password;
    @Nullable
    String image;
}
