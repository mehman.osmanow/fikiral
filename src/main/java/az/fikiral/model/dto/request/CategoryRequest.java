
package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;
/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryRequest {

   @NotBlank(message = "Invalid Name: Empty name")
   @Size(max = 25, min = 2, message = "Invalid Name: Must be of 2 - 25 characters")
   @Schema(name = "name", requiredMode = Schema.RequiredMode.REQUIRED, description = "Category must be unique")
   String name;

   @NotBlank(message = "Invalid Name: Empty description")
   @Size(max = 25, min = 2, message = "Invalid Name: Must be of 2 - 25 characters")
   @Schema(name = "slug", requiredMode = Schema.RequiredMode.REQUIRED, description = "Category must be unique")
   String slug;

}