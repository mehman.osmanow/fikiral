package az.fikiral.model.dto.request;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 24.02.24
 * @project fikiral
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LikeCommentRequest {
    Long userId;
    Long commentId;
    boolean liked;

}
