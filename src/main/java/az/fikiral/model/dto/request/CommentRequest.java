package az.fikiral.model.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 14.02.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CommentRequest {
    String content;
    Long userId;
    Long postId;

}
