package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 16.03.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ChangePasswordRequest {

    @NotNull
    @Email
    @Schema(name = "email", example = "test@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    String email;

    @NotBlank
    @Schema(name = "newPassword", example = "Aa123123", requiredMode = Schema.RequiredMode.REQUIRED)
    String newPassword;
}
