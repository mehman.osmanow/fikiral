package az.fikiral.model.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @author Mehman Osmanov 07.02.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequest {

    @NotBlank
    @Schema(name = "userName", example = "john07", requiredMode = Schema.RequiredMode.REQUIRED)
    String userName;

    @NotBlank
    @Email
    @Schema(name = "gmail", example = "example@gmail.com", requiredMode = Schema.RequiredMode.REQUIRED)
    String gmail;

    @NotBlank
    @Size(min = 8)
    @Schema(name = "password", example = "example123", requiredMode = Schema.RequiredMode.REQUIRED)
    String password;

    @Schema(name = "categories", example = "[1, 3, 6]",requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    List<Long> categories;

}
