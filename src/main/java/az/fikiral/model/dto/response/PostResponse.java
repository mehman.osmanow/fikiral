package az.fikiral.model.dto.response;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author Mehman Osmanov 05.02.24
 * @project fikiral
 */
@Setter
@Getter
public class PostResponse {
    Long id;
    String content;
    int likeCount;
    int commentCount;
    LocalDateTime publishedAt;
    UserResponse user;
    CategoryResponse category;

}
