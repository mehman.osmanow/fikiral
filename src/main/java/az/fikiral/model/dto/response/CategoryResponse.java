
package az.fikiral.model.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;
/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryResponse {
   Long id;
   String name;
   String slug;
   int postCount;
}
