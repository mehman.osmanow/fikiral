package az.fikiral.model.dto.response;


import az.fikiral.model.enums.RoleType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Mehman Osmanov 07.02.24
 * @project fikiral
 */
@ApiResponses
@Data
public class UserResponse {
    Long id;
    @ApiModelProperty(notes = "User name", example = "john07")
    String userName;
    @ApiModelProperty(notes = "Email", example = "example@gmail.com")
    String gmail;
    @ApiModelProperty(notes = "Image", example = "url")
    String image;
    int postCount;
    boolean activated;
    LocalDateTime registeredAt;

    @Enumerated(value = EnumType.STRING)
    RoleType roleType;

}