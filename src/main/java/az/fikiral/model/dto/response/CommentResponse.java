package az.fikiral.model.dto.response;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Mehman Osmanov 14.02.24
 * @project fikiral
 */
@Data
public class CommentResponse {
    Long id;
    String content;
    int likeCount;
    LocalDateTime publishedAt;
    UserResponse user;

}
