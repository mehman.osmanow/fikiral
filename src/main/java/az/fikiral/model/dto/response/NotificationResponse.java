package az.fikiral.model.dto.response;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class NotificationResponse {
    Long id;
    String action;
    String actionOwnerName;
    String actionOwnerImage;

    PostResponse post;
}
