package az.fikiral.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mehman Osmanov 15.02.24
 * @project fikiral
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthResponse {
    UserResponse userResponse;
    TokenResponse tokenResponse;
}
