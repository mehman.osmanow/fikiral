package az.fikiral.model.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Mehman Osmanov 23.02.24
 * @project fikiral
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserFullResponse {
    Long id;
    @ApiModelProperty(notes = "User name", example = "john07")
    String userName;
    @ApiModelProperty(notes = "Email", example = "example@gmail.com")
    String gmail;
    @ApiModelProperty(notes = "Image", example = "url")
    String image;
    LocalDateTime registeredAt;
    List<Long> likedPostsIDs;
    List<Long> disLikedPostsIDs;
    List<Long> savedPostsIDs;
    List<Long> categoryIds;
}
