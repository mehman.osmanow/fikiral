package az.fikiral.model;

import lombok.Getter;

/**
 * @author Mehman Osmanov 23.03.24
 * @project fikiral
 */
@Getter
public class Constant {


    public static final String VERIFICATION_MSG =
                """
                <div>
                <h3 Email Verification📧\n ></h3>
                <p>
                Dear user, %s 🥰,</p>
                <p>Please click the link below to verify your email address🤩\n
                </p>
                <p><img src=\"https://fikiral-bkt.s3.eu-central-1.amazonaws.com/verification/email-verification\"></p>
                <a href=\"https://fikiral-app.onrender.com/fikiral/v1/verify/%s\" target=\"_blank\">👉 click to verify your email</a>
                </div>""";


    public static final String CHANGE_PASSWORD_LITERAL =
                """
                <div>
                <p><img src=\"https://fikiral-bkt.s3.eu-central-1.amazonaws.com/verification/forgot-password\"></p>
                <a href=\"https://fikiral.az/auth\" target=\"_blank\"> 👉 click to change your password</a>
                </div>""";
}
