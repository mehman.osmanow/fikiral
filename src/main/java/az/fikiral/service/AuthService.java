package az.fikiral.service;


import az.fikiral.model.dto.request.ChangePasswordRequest;
import az.fikiral.model.dto.request.LoginRequest;
import az.fikiral.model.dto.request.UserRequest;
import az.fikiral.model.dto.response.AuthResponse;
import az.fikiral.model.dto.response.TokenResponse;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.IOException;

public interface AuthService {
    AuthResponse login(LoginRequest request);
    TokenResponse refreshJWT(UserDetails userDetails);
    void registration(UserRequest user) throws IOException;

    String forgotPassword(String email) throws IOException;

    //    void resetPassword();
    void changePassword(ChangePasswordRequest request);
}
