package az.fikiral.service;

import az.fikiral.model.dto.request.NotificationRequest;
import az.fikiral.model.dto.response.NotificationResponse;

import java.util.List;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
public interface NotificationService {
    void createNotification(NotificationRequest request);
    List<NotificationResponse> getAllNotification(Long userId);

    void deleteNotification(Long id);

}
