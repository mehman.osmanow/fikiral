package az.fikiral.service;

import az.fikiral.model.dto.request.PostRequest;
import az.fikiral.model.dto.response.PostResponse;

import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

public interface PostService {

    void createPost(PostRequest postRequest);
    List<Long> savePost(Long userId, Long postId);

    void acceptPost(Long id);

    PostResponse getById(Long id);

    List<PostResponse> getAllAcceptedPosts();
    List<PostResponse> getAllInAcceptedPosts();
    List<PostResponse> getPopularPosts(int limit);
    List<PostResponse> getPostsByCategorySlug(String slug);

    List<List<PostResponse>> getPostsByCategoryIds(List<Long> categories);

    List<PostResponse> findPostsByContent(String content);
    void updatePost(String content, Long id);
    void deletePost(Long id);
}
