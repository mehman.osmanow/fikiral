package az.fikiral.service;

import az.fikiral.model.dto.request.CategoryRequest;
import az.fikiral.model.dto.response.CategoryResponse;

import java.util.List;

/**
 * @author Mehman Osmanov 13.02.24
 * @project fikiral
 */
public interface CategoryService {
    void createCategory(CategoryRequest categoryRequest);
    CategoryResponse getCategoryById(Long id);
    List<CategoryResponse> getAllCategory();
    List<CategoryResponse> getSavedCategoryByUserId(Long userId);
    void updateCategory(Long id ,CategoryRequest request);
    void deleteCategoryById(Long id);

}
