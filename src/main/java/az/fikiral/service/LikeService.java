package az.fikiral.service;


import az.fikiral.model.dto.request.LikeCommentRequest;
import az.fikiral.model.dto.request.LikePostRequest;

public interface LikeService {
    void likePost(LikePostRequest request);
    void likeComment(LikeCommentRequest request);
    int getLikeCountByPostId(Long postId, boolean liked);
    int getLikeCountByCommentId(Long commentId);
}
