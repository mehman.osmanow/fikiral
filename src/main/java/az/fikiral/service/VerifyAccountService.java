package az.fikiral.service;

public interface VerifyAccountService {
    String verify(String token);
    boolean isActivated(String gmail);
}
