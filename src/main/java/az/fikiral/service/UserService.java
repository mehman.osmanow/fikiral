package az.fikiral.service;

import az.fikiral.model.dto.request.UserUpdateRequest;
import az.fikiral.model.dto.response.UserFullResponse;
import az.fikiral.model.dto.response.UserResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */
public interface UserService {
    List<UserResponse> getAllUsers();

    UserFullResponse getUserById(Long id);
    List<UserResponse> getAllUser();
    boolean checkUserGmail(String gmail);
    boolean checkUserName(String userName);
    void updateUser(UserUpdateRequest userRequest, Long id);
    void  deleteUserById(Long id);
    void blockedUnlocked(Long id);
    void addImage(MultipartFile image, Long id) throws IOException;

}
