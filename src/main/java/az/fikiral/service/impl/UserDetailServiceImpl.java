package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.model.entity.User;
import az.fikiral.repository.UserRepository;
import az.fikiral.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String mail) throws NotFoundException {
        Optional<User> userOptional = userRepository.findByGmail(mail);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            UserPrincipal userPrincipal = new UserPrincipal();
            userPrincipal.setMail(user.getGmail());
            userPrincipal.setPassword(user.getPassword());
            return userPrincipal;
        } else {
            throw new NotFoundException(mail + " user not found");
        }
    }
}
