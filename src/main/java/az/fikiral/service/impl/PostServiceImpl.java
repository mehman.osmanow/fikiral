package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.mapper.PostMapper;
import az.fikiral.model.dto.request.PostRequest;
import az.fikiral.model.dto.response.PostResponse;
import az.fikiral.model.entity.Post;
import az.fikiral.repository.CategoryRepository;
import az.fikiral.repository.NotificationRepository;
import az.fikiral.repository.PostRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.service.CommentService;
import az.fikiral.service.LikeService;
import az.fikiral.service.NotificationService;
import az.fikiral.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

@Slf4j
@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final PostRepository postRepository;
    private final PostMapper postMapper;
    private final LikeService likeService;
    private final CommentService commentService;
    private final NotificationRepository notificationRepository;

    @Override
    public void createPost(PostRequest postRequest) {
        log.info("Start to create post");
        var user = userRepository.findById(postRequest.getUserId()).orElseThrow(
                () -> new NotFoundException("There is no user with this id"));
        var category = categoryRepository.findById(postRequest.getCategoryId()).orElseThrow(
                () -> new NotFoundException("There is no category with this id"));
        log.info("Create a new post");
        Post post = Post.builder()
                .user(user)
                .category(category)
                .content(postRequest.getContent()).build();
        postRepository.save(post);
        log.info("Post created");
    }

    @Override
    public List<Long> savePost(Long userId, Long postId) {
        log.info("Start to save post");
        var user = userRepository.findById(userId).orElseThrow(
                () -> new NotFoundException("There is no user with this id"));
        var post = postRepository.findById(postId).orElseThrow(
                () -> new NotFoundException("There is no post with id " + postId));
        if (user.getSavedPosts().contains(post)) {
            user.getSavedPosts().remove(post);
        } else {
            user.getSavedPosts().add(post);
        }
        userRepository.save(user);
        log.info("Post saved");
        return postRepository.getAllSavedPostIDs(userId);

    }

    @Override
    public List<PostResponse> getAllAcceptedPosts() {
        log.info("Get all accepted posts");
        return postRepository.findAllByAccepted(true).stream()
                .map(post -> {
                    int commentCount = getCommentCount(post.getId());
                    int likeCount = getLikeOrDislikeCount(post.getId(),true)
                            - getLikeOrDislikeCount(post.getId(),false);
                    return postMapper.entityToResponse(post, likeCount, commentCount);
                }).toList();
    }

    @Override
    public List<PostResponse> getAllInAcceptedPosts() {
        log.info("Get all in-accepted posts");
        return postRepository.findAllByAccepted(false).stream()
                .map(post -> {
                    int commentCount = getCommentCount(post.getId());
                    int likeCount = getLikeOrDislikeCount(post.getId(),true)
                            - getLikeOrDislikeCount(post.getId(),false);
                    return postMapper.entityToResponse(post, likeCount, commentCount);
                }).toList();
    }



    @Override
    public PostResponse getById(Long id) {
        log.info("Get all  posts");
        var post = postRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no post with id " + id));
        int commentCount = getCommentCount(post.getId());
        int likeCount = getLikeOrDislikeCount(post.getId(),true)
                - getLikeOrDislikeCount(post.getId(),false);
        return postMapper.entityToResponse(post, likeCount, commentCount);
    }

    @Override
    public List<PostResponse> getPopularPosts(int limit) {
        log.info("Get popular posts");
        return postRepository.getPopularPosts(limit).stream()
                .map(post -> {
                    int commentCount = getCommentCount(post.getId());
                    int likeCount = getLikeOrDislikeCount(post.getId(),true)
                            - getLikeOrDislikeCount(post.getId(),false);
                    return postMapper.entityToResponse(post, likeCount, commentCount);
                }).toList();
    }

    @Override
    public List<PostResponse> getPostsByCategorySlug(String slug) {
        log.info("Get post by category slug");
        var category = categoryRepository.getCategoryBySlug(slug).orElseThrow(
                () -> new NotFoundException("There is no category with name " + slug));
        return postRepository.getPostsByCategory_Id(category.getId()).stream()
                .map(post -> {
                    int commentCount = getCommentCount(post.getId());
                    int likeCount = getLikeOrDislikeCount(post.getId(),true)
                            - getLikeOrDislikeCount(post.getId(),false);
                    return postMapper.entityToResponse(post, likeCount, commentCount);
                }).toList();
    }

    @Override
    public List<List<PostResponse>> getPostsByCategoryIds(List<Long> categories) {
        log.info("Get posts by category ID");

        return categories.stream()
                .map(postRepository::getPostsByCategory_Id)
                .map(postsByCategory -> postsByCategory.stream()
                        .map(post -> {
                            int commentCount = getCommentCount(post.getId());
                            int likeCount = getLikeOrDislikeCount(post.getId(),true)
                                    - getLikeOrDislikeCount(post.getId(),false);
                            return postMapper.entityToResponse(post, likeCount, commentCount);
                        }).toList())
                .collect(Collectors.toList());
    }

    @Override
    public List<PostResponse> findPostsByContent(String content) {
        log.info("Finding post");
        return postRepository.getPostByContentContainingIgnoreCase(content).stream()
                .map(post -> {
                    int commentCount = getCommentCount(post.getId());
                    int likeCount = getLikeOrDislikeCount(post.getId(),true)
                            - getLikeOrDislikeCount(post.getId(),false);
                    return postMapper.entityToResponse(post, likeCount, commentCount);
                }).toList();
    }

    private int getLikeOrDislikeCount(Long postId, boolean like) {
        log.info("Getting like count");
        return likeService.getLikeCountByPostId(postId, like);
    }

    private int getCommentCount(Long postId) {
        log.info("Getting comment count");
        return commentService.getCommentCountByPostId(postId);
    }

    @Override
    public void updatePost(String content, Long id) {
        log.info("Updating post");
        Post post = postRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no post with id " + id));
        post.setContent(content);
        postRepository.save(post);
    }

    @Override
    public void deletePost(Long id) {
        log.warn("Deleting post");
        var notifications = notificationRepository.findAllByPostId(id);
        notificationRepository.deleteAll(notifications);
        var post = postRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no post with id " + id));
        postRepository.delete(post);
    }

    @Override
    public void acceptPost(Long id){
        log.info("Accepting post");
        var post = postRepository.findById(id).orElseThrow();
        post.setAccepted(true);
        postRepository.save(post);

    }

}
