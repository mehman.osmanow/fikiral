package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.repository.UserRepository;
import az.fikiral.security.JwtTokenProvider;
import az.fikiral.service.VerifyAccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class VerifyAccountServiceImpl implements VerifyAccountService {

    private final JwtTokenProvider tokenProvider;
    private final UserRepository userRepository;

    private final static String SUCCESS_MSG = "Email successfully verified🎉🎊✅";
    private final static String FAIL_MSG = "Verification failed🙄";

    @Override
    public String verify(String token) {
        log.info("Start to verify user account");
        if (tokenProvider.validateToken(token)) {
            var email = tokenProvider.getMailFromJWT(token);
            var user = userRepository.findByGmail(email).orElseThrow(
                    () -> new NotFoundException(String.format("User not found with %s email ", email)));
            user.setActivated(true);
            userRepository.save(user);
            return SUCCESS_MSG;
        }
        return FAIL_MSG;
    }

    @Override
    public boolean isActivated(String gmail) {
        var user = userRepository.findByGmailAndActivatedIsTrue(gmail);
        return user.isPresent();
    }
}
