
package az.fikiral.service.impl;

import az.fikiral.exception.AccessForbidden;
import az.fikiral.exception.AlreadyExistsException;
import az.fikiral.exception.NotFoundException;
import az.fikiral.exception.WrongPasswordException;
import az.fikiral.mapper.UserMapper;
import az.fikiral.model.dto.request.ChangePasswordRequest;
import az.fikiral.model.dto.request.UserRequest;
import az.fikiral.model.dto.response.AuthResponse;
import az.fikiral.model.dto.response.UserResponse;
import az.fikiral.model.entity.Category;
import az.fikiral.model.entity.User;
import az.fikiral.model.enums.RoleType;
import az.fikiral.model.enums.TokenType;
import az.fikiral.model.dto.request.LoginRequest;
import az.fikiral.model.dto.response.TokenResponse;
import az.fikiral.repository.CategoryRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.security.JwtTokenProvider;
import az.fikiral.security.PasswordEncoder;
import az.fikiral.security.UserPrincipal;
import az.fikiral.service.AuthService;
import az.fikiral.service.EmailService;
import jakarta.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final UserMapper userMapper;
    private final EmailService emailService;

    @Override
    public void registration(UserRequest userRequest) throws IOException {
        log.info("Start to find user by gmail if it is already exist");
        userRepository.findByGmail(userRequest.getGmail()).ifPresent(student -> {
            throw new AlreadyExistsException("İstifadəçi '%s' e-poçt ilə artıq mövcuddur".formatted(userRequest.getGmail()));
        });
        log.info("Start to find user by userName if it is already exist");
        userRepository.findByUserName(userRequest.getUserName()).ifPresent(student -> {
            throw new AlreadyExistsException("İstifadəçi artıq '%s' istifadəçi adı ilə mövcuddur".formatted(userRequest.getUserName()));
        });
        log.info("Starting registration");
        List<Category> categories = categoryRepository.findAllById(userRequest.getCategories());
        User user = userMapper.requestToEntity(userRequest);
        user.getInterestedCategory().addAll(categories);
        user.setPassword(passwordEncoder.passwordEncode(userRequest.getPassword()));
        user.setRoleType(RoleType.USER);
        userRepository.save(user);

        //todo sending mail
        var userDetails = new UserPrincipal();
        userDetails.setMail(user.getGmail());
        userDetails.setPassword(user.getPassword());
        String token = jwtTokenProvider.generateToken(userDetails, TokenType.ACCESS_TOKEN);
        try {
            log.info("Sending an email to verification");
            emailService.sendVerificationToEmail(token, user.getGmail());
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public AuthResponse login(LoginRequest request) {
        log.info("Start to find user by gmail if it is already exist");
        var user = userRepository.findByGmail(request.getGmail()).orElseThrow(
                () -> new NotFoundException("İstifadəçi e-poçt '%s' ilə tapılmadı".formatted(request.getGmail())));
        if (!user.isActivated()) {
            throw new AccessForbidden("İstifadəçi aktivləşdirilməyib");
        } else if (user.isDeleted()) {
            throw new AccessForbidden("İstifadəçi silinib");
        } else if (user.isBlocked()) {
            throw new AccessForbidden("İstifadəçi bloklanıb");
        }
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            request.getGmail(),
                            request.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            TokenResponse tokenResponse = new TokenResponse();
            tokenResponse.setAccessToken(jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal(), TokenType.ACCESS_TOKEN));
            tokenResponse.setRefreshToken(jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal(), TokenType.REFRESH_TOKEN));

            UserResponse userResponse = userMapper.entityToResponse(user);
            return AuthResponse.builder()
                    .userResponse(userResponse)
                    .tokenResponse(tokenResponse).build();
        } catch (RuntimeException ex) {
            log.error("Wrong password entered");
            throw new WrongPasswordException("Daxil edilmiş parol yanlışdır");
        }
    }

    @Override
    public TokenResponse refreshJWT(UserDetails userDetails) {
        log.info("Start to refresh JWT");
        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(jwtTokenProvider.generateToken(userDetails, TokenType.ACCESS_TOKEN));
        tokenResponse.setRefreshToken(jwtTokenProvider.generateToken(userDetails, TokenType.REFRESH_TOKEN));
        return tokenResponse;
    }

    @Override
    public String forgotPassword(String email) {
        userRepository.findByGmail(email).orElseThrow(() -> new NotFoundException("İstifadəçi e-poçt '%s' ilə tapılmadı".formatted(email)));
        try {
            emailService.sendChangePassword(email);
        } catch (MessagingException | IOException e) {
            throw new RuntimeException("Unable to send set password email pls try again");
        }
        return "Şifrəni dəyişmək üçün e-poçtunuzu yoxlayın";
    }


    @Override
    public void changePassword(ChangePasswordRequest request) {
        log.info("Start to change password");
        var user = userRepository.findByGmail(request.getEmail()).orElseThrow(
                () -> new NotFoundException("User not found with email: " + request.getEmail()));
        user.setPassword(passwordEncoder.passwordEncode(request.getNewPassword()));
        userRepository.save(user);
    }

}
