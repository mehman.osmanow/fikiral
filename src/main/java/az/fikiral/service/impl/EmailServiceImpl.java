package az.fikiral.service.impl;

import az.fikiral.repository.UserRepository;
import az.fikiral.service.EmailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.io.IOException;

import static az.fikiral.model.Constant.CHANGE_PASSWORD_LITERAL;
import static az.fikiral.model.Constant.VERIFICATION_MSG;


@Service
@Slf4j
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final UserRepository userRepository;
    private final JavaMailSender javaMailSender;

    @Override
    public void sendMail(String to, String subject, String content) throws IOException {
        log.info("Sending email to the {}", to);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        javaMailSender.send(message);
    }

    @Override
    public void sendVerificationToEmail(String token, String email) throws MessagingException {
        MimeMessage message=javaMailSender.createMimeMessage();
        MimeMessageHelper helper=new MimeMessageHelper(message,true);
        helper.setTo(email);
        String userName = null;
        var userOp = userRepository.findByGmail(email);
        if(userOp.isPresent()){
            userName = userOp.get().getUserName();
        }
        helper.setSubject("Email Verification");
        helper.setText(VERIFICATION_MSG.formatted(userName,token),true);
        javaMailSender.send(message);
    }

    @Override
    public void sendChangePassword(String email) throws MessagingException, IOException {
        MimeMessage message=javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper=new MimeMessageHelper(message);
        mimeMessageHelper.setTo(email);
        mimeMessageHelper.setSubject("Change Password");
        mimeMessageHelper.setText(CHANGE_PASSWORD_LITERAL,true);
        javaMailSender.send(message);
    }


}

