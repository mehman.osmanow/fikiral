package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.mapper.CommentMapper;
import az.fikiral.model.dto.request.CommentRequest;
import az.fikiral.model.dto.response.CommentResponse;
import az.fikiral.model.entity.Comment;
import az.fikiral.repository.CommentRepository;
import az.fikiral.repository.PostRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.service.CommentService;
import az.fikiral.service.LikeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentMapper commentMapper;
    private final LikeService likeService;

    @Override
    public void createComment(CommentRequest request) {
        var user = userRepository.findById(request.getUserId()).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + request.getUserId()));
        var post = postRepository.findById(request.getPostId()).orElseThrow(
                () -> new NotFoundException("There is no post with id " + request.getPostId()));
        Comment comment = Comment.builder()
                .user(user)
                .post(post)
                .content(request.getContent()).build();
        commentRepository.save(comment);
        log.info("Created new comment");
    }

    @Override
    public List<CommentResponse> getAllComments(Long postId) {
        return commentRepository.getComment(postId).stream()
                .map(comment -> {
                    int likeCount = getLikeCountByCommentId(comment.getId());
                    return commentMapper.entityToResponse(comment, likeCount);
                }).toList();
    }

    @Override
    public int getCommentCountByPostId(Long postId) {
        return commentRepository.countAllByPostId(postId);
    }

    private int getLikeCountByCommentId(Long commentId) {
        return likeService.getLikeCountByCommentId(commentId);
    }

    @Override
    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}
