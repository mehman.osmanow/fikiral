package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.mapper.NotificationMapper;
import az.fikiral.model.dto.request.NotificationRequest;
import az.fikiral.model.dto.response.NotificationResponse;
import az.fikiral.repository.NotificationRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.service.NotificationService;
import az.fikiral.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mehman Osmanov 21.03.24
 * @project fikiral
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final PostService postService;
    private final UserRepository userRepository;
    private final NotificationRepository repository;
    private final NotificationMapper mapper;

    @Override
    public void createNotification(NotificationRequest request) {
        log.info("Create notification");
        var notification = mapper.requestToEntity(request);
        notification.setAction(request.getAction().toUpperCase());
        repository.save(notification);
    }

    @Override
    public List<NotificationResponse> getAllNotification(Long userId) {
        log.info("Getting all notifications");
        var notifications = repository.findAllByPostOwnerId(userId);

        List<NotificationResponse> notificationResponses = new ArrayList<>();
        if (!notifications.isEmpty()) {
            for (var notification : notifications) {
                var actionOwnerId = notification.getActionOwnerId();
                var actionOwner = userRepository.findById(actionOwnerId).
                        orElseThrow(() -> new NotFoundException("There is no user with this id=" + actionOwnerId));
                var notificationResponse = mapper.entityToResponse(notification);

                notificationResponse.setPost(postService.getById(notification.getPostId()));
                notificationResponse.setActionOwnerName(actionOwner.getUserName());
                notificationResponse.setActionOwnerImage(actionOwner.getImage());

                notificationResponses.add(notificationResponse);
            }
        }
        return notificationResponses;
    }

    @Override
    public void deleteNotification(Long id) {
        log.info("Delete notification");
        repository.deleteById(id);

    }
}
