
package az.fikiral.service.impl;

import az.fikiral.exception.AlreadyExistsException;
import az.fikiral.exception.NotFoundException;
import az.fikiral.mapper.CategoryMapper;
import az.fikiral.model.dto.request.CategoryRequest;
import az.fikiral.model.dto.response.CategoryResponse;
import az.fikiral.model.entity.Category;
import az.fikiral.repository.CategoryRepository;
import az.fikiral.service.CategoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @author Mehman Osmanov 06.02.24
 * @project fikiral
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    @Override
    public void createCategory(CategoryRequest request) {
        if (categoryRepository.existsByName(request.getName())) {
            log.error("Category name - " + request.getName() + " already exist!!!");
            throw new AlreadyExistsException("Category name-" + request.getName() + "already exist!!!");
        }
        Category category = categoryRepository.save(categoryMapper.requestToEntity(request));
        log.info("Created new category \n {}", category);
    }

    @Override
    public List<CategoryResponse> getAllCategory() {
        log.info("Start to find all categories");
        return categoryRepository.findAll().stream().map(categoryMapper::entityToResponse).toList();
    }

    @Override
    public List<CategoryResponse> getSavedCategoryByUserId(Long userId) {
        return categoryRepository.getSavedCategoriesByUserId(userId)
                .stream().map(categoryMapper::entityToResponse).toList();
    }

    @Override
    public CategoryResponse getCategoryById(Long id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category not found"));
        log.info("Getting category by id" + category.toString());
        return categoryMapper.entityToResponse(category);
    }



    @Override
    public void updateCategory(Long id, CategoryRequest request) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category id not found.ID: " + id));
        Category newCategory = categoryMapper.requestToEntity(request);
        newCategory.setId(category.getId());
        categoryRepository.save(newCategory);
        log.info("Category successfully updated");
    }

    @Override
    public void deleteCategoryById(Long id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Category id not found"));
        categoryRepository.delete(category);
        log.info("Category has been deleted successfully.Deleted category id {}", id);
    }

}
