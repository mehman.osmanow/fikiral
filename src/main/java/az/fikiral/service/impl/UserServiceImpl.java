package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.mapper.UserMapper;
import az.fikiral.model.dto.request.UserUpdateRequest;
import az.fikiral.model.dto.response.UserFullResponse;
import az.fikiral.model.dto.response.UserResponse;
import az.fikiral.model.entity.Category;
import az.fikiral.model.entity.User;
import az.fikiral.repository.CategoryRepository;
import az.fikiral.repository.LikeRepository;
import az.fikiral.repository.PostRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.service.UserService;
import az.fikiral.util.FileUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static az.fikiral.constant.Constants.BUCKET_NAME;
import static az.fikiral.constant.Constants.OBJECT_KEY;


@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final LikeRepository likeRepository;
    private final PostRepository postRepository;
    private final CategoryRepository categoryRepository;
    private final FileUtil fileUtil;


    @Override
    @SneakyThrows
    public void addImage(MultipartFile image, Long id) {
        log.info("Start uploading profile photo");
        var user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + id));
        String url = fileUtil.uploadImage(BUCKET_NAME, OBJECT_KEY.concat(image.getOriginalFilename()), image);
        user.setImage(url);
        userRepository.save(user);
    }

    @Override
    public List<UserResponse> getAllUser() {
        log.info("Start to get all accounts");
        List<User> list = userRepository.findAllByActivatedIsTrue();
        return list.stream().map(userMapper::entityToResponse).toList();
    }

    @Override
    public List<UserResponse> getAllUsers() {
        log.info("Start to get all accounts");
        List<User> list = userRepository.findAllByDeletedFalse();
        return list.stream().map(userMapper::entityToResponse).toList();
    }

    @Override
    public UserFullResponse getUserById(Long id) {
        log.info("Start to find user account by ID");
        var user = userRepository.findByIdAndActivatedIsTrue(id).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + id));
        var userResponse = userMapper.entityToLoginResponse(user);
        userResponse.setLikedPostsIDs(likeRepository.getAllLikedOrDislikedPostsIDs(id, true));
        userResponse.setDisLikedPostsIDs(likeRepository.getAllLikedOrDislikedPostsIDs(id, false));
        userResponse.setSavedPostsIDs(postRepository.getAllSavedPostIDs(id));
        userResponse.setCategoryIds(categoryRepository.getInterestedCategoriesId(id));
        return userResponse;
    }

    @Override
    public boolean checkUserGmail(String gmail) {
        log.info("Check the user's gmail to see if it exists.");
        return userRepository.existsByGmail(gmail);
    }

    @Override
    public boolean checkUserName(String userName) {
        log.info("Check the username to see if it exists.");
        return userRepository.existsByUserName(userName);
    }

    @Override
    public void updateUser(UserUpdateRequest userUpdateRequest, Long id) {
        log.info("Start to update account");
        var user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + id));
        if (userUpdateRequest.getCategories() != null) {
            List<Category> categories = categoryRepository.findAllById(userUpdateRequest.getCategories());
            user.getInterestedCategory().addAll(categories);
        }
        if (userUpdateRequest.getUserName() != null)
            user.setUserName(userUpdateRequest.getUserName());
        if (userUpdateRequest.getGmail() != null)
            user.setGmail(userUpdateRequest.getGmail());
        userRepository.save(user);
    }

    @Override
    public void deleteUserById(Long id) {
        log.info("Start to delete account");
        var user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + id));
        user.setDeleted(true);
        user.setActivated(false);
        userRepository.save(user);
    }

    @Override
    public void blockedUnlocked(Long id) {
        log.info("Start to restore account");
        var user = userRepository.findById(id).orElseThrow(
                () -> new NotFoundException("There is no user with this id=" + id));
        if (user.isBlocked()) {
            user.setBlocked(false);
            user.setActivated(true);
        } else {
            user.setBlocked(true);
            user.setActivated(false);
        }

        userRepository.save(user);
    }

}
