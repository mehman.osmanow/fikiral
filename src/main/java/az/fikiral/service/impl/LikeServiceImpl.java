package az.fikiral.service.impl;

import az.fikiral.exception.NotFoundException;
import az.fikiral.model.dto.request.LikeCommentRequest;
import az.fikiral.model.dto.request.LikePostRequest;
import az.fikiral.model.entity.Like;
import az.fikiral.repository.CommentRepository;
import az.fikiral.repository.LikeRepository;
import az.fikiral.repository.PostRepository;
import az.fikiral.repository.UserRepository;
import az.fikiral.service.LikeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Mehman Osmanov 24.02.24
 * @project fikiral
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LikeServiceImpl implements LikeService {

    private final LikeRepository likeRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    @Override
    public void likePost(LikePostRequest request) {
        var user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new NotFoundException("There is no user with this id=" + request.getUserId()));
        var post = postRepository.findById(request.getPostId())
                .orElseThrow(() -> new NotFoundException("There is no post with this id=" + request.getPostId()));
        Like like = likeRepository.findByUserAndPost(user, post)
                .orElse(new Like(user, post, request.isLiked()));
        if (request.isLiked() == like.isLiked()){
            likeRepository.save(like);
        }else {
            like.setLiked(request.isLiked());
            likeRepository.save(like);
        }
        log.info("Post was liked");
    }


    public void likeComment(LikeCommentRequest request) {
        var user = userRepository.findById(request.getUserId())
                .orElseThrow(() -> new NotFoundException("There is no user with this id=" + request.getUserId()));

        var comment = commentRepository.findById(request.getCommentId())
                .orElseThrow(() -> new NotFoundException("There is no comment with this id=" + request.getCommentId()));
        Like like = likeRepository.findByUserAndComment(user, comment)
                .orElse(new Like(user, comment, request.isLiked()));
        if (request.isLiked() == like.isLiked()){
            likeRepository.save(like);
        }else {
            like.setLiked(request.isLiked());
            likeRepository.save(like);
        }
        log.info("Comment was liked");
    }


    @Override
    public int getLikeCountByPostId(Long postId, boolean liked) {
        return likeRepository.countAllByPostIdAndLiked(postId, liked);
    }

    @Override
    public int getLikeCountByCommentId(Long commentId) {
        return likeRepository.countAllByCommentIdAndLikedIsTrue(commentId);
    }

}