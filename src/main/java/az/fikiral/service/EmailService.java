package az.fikiral.service;

import jakarta.mail.MessagingException;

import java.io.IOException;

public interface EmailService {
    void sendMail(String to, String subject, String content) throws IOException;

    void sendVerificationToEmail(String token, String mail) throws MessagingException;

    void sendChangePassword(String email) throws MessagingException, IOException;
}
