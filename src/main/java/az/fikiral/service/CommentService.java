package az.fikiral.service;

import az.fikiral.model.dto.request.CommentRequest;
import az.fikiral.model.dto.response.CommentResponse;

import java.util.List;

/**
 * @author Mehman Osmanov 16.02.24
 * @project fikiral
 */
public interface CommentService {
    void createComment(CommentRequest request);
    List<CommentResponse> getAllComments(Long postId);
    int getCommentCountByPostId(Long postId);

    void deleteComment(Long id);
}