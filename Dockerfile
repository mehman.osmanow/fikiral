FROM openjdk
COPY build/libs/fikiral-0.0.1-SNAPSHOT.jar .
CMD ["java", "-jar", "fikiral-0.0.1-SNAPSHOT.jar"]